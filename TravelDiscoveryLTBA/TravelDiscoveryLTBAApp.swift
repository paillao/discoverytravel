//
//  TravelDiscoveryLTBAApp.swift
//  TravelDiscoveryLTBA
//
//  Created by Jorge Luis Paillao on 19-09-21.
//

import SwiftUI

@main
struct TravelDiscoveryLTBAApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
